/**
 * Created by Apricode on 10/18/2017.
 */
export default function(config){
    (function (config) {

        angular.module('apricode-rest', [])
            .service('restService', ["connection", "$q",restService]);


        function restService(connection, $q) {

            function setField(object, value, path) {
                var a = path.split('.');
                var o = object;
                for (var i = 0; i < a.length - 1; i++) {
                    var n = a[i];
                    if (n in o) {
                        o = o[n];
                    } else {
                        o[n] = {};
                        o = o[n];
                    }
                }
                o[a[a.length - 1]] = value;
            }


            let service = {

                //default is post unless modelsToUpdate has ids
                save: function (formsModels) {
                    return Object.keys(formsModels).reduce((p, modelName) => {
                        let model = formsModels[modelName];
                        return p.then(() => {
                            if (model._id) {
                                return connection.sendPut(`${config.restPrefix}${modelName}/${model._id}`, model)
                            }
                            else {
                                return connection.sendPost(`${config.restPrefix}${modelName}`, model)
                            }
                        });
                    }, $q.when());
                },
                update: (modelName, id, data) => {
                    return connection.sendPut(`${config.restPrefix}${modelName}/${id}`, data);
                },
                new: (modelName, data) => {
                    connection.sendPost(`${config.restPrefix}${modelName}`, data);
                },
                get: (modelName, {filters, projection, populate, languageId,paging,sort} = {}) => {
                    return connection.sendGet(`${config.restPrefix}${modelName}?${filters ? 'filters=' + JSON.stringify(filters) + '&' : ''}`
                        + `${projection ? 'project=' + JSON.stringify(projection) + '&' : ''}`
                        + `${populate ? 'populate=' + JSON.stringify(populate) + '&' : ''}`
                        + `${paging ? 'page=' + paging.page + '&pageSize=' + paging.pageSize + '&' : ''}`
                        + `${sort ? 'sort=' + sort.field + '&order=' + sort.order + '&' : ''}`
                        + `${languageId ? 'languageId=' + languageId : ''}`);
                },
                getById: (modelName, id, {projection, populate, languageId} = {}) => {
                    return connection.sendGet(`${config.restPrefix}${modelName}/${id}?`
                        + `${projection ? 'project=' + JSON.stringify(projection) + '&' : ''}`
                        + `${populate ? 'populate=' + JSON.stringify(populate) + '&' : ''}`
                        + `${languageId ? 'languageId=' + languageId : ''}`);
                },
                configure: function (_config) {
                    config = _config;
                    connection.setConfig(config)
                }
            };

            return service;
        }


    })(config);
}